<?php

namespace App\DataFixtures;

use App\Entity\Favorites;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavoritesFixtures extends Fixture implements DependentFixtureInterface
{
    private static $favorites = [];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $namesFavorites[] = [
            'picture_id' => 't3_7e2x3e',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8wgone',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qc84c',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8ouvwv',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8sh3vu',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8widua',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qaaoq',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8nr3ju',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_7x1mc1',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8gncs3',
            'user' => $this->getReference(UserFixtures::USER_ONE)
        ];

        /*user2*/

        $namesFavorites[] = [
            'picture_id' => 't3_7e2x3e',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8wgone',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qc84c',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8ouvwv',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8sh3vu',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8widua',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qaaoq',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8nr3ju',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_7x1mc1',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8gncs3',
            'user' => $this->getReference(UserFixtures::USER_TWO)
        ];

        /*user3*/

        $namesFavorites[] = [
            'picture_id' => 't3_7e2x3e',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8wgone',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qc84c',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8ouvwv',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8sh3vu',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8widua',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8qaaoq',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8nr3ju',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_7x1mc1',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8gncs3',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];
        $namesFavorites[] = [
            'picture_id' => 't3_8tr6z7',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];

        $namesFavorites[] = [
            'picture_id' => 't3_8v27o0',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];

        $namesFavorites[] = [
            'picture_id' => 't3_8wa3ls',
            'user' => $this->getReference(UserFixtures::USER_THREE)
        ];

        foreach ($namesFavorites as $item) {
            $favorites = new Favorites();
            $user = $item['user'];
            $favorites
                ->setCreatedAt(new \DateTime())
                ->setPictureId($item['picture_id'])
                ->setUser($user)
            ;
            $manager->persist($favorites);
            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }

    public static function getFavorites() {
        return self::$favorites;
    }
}