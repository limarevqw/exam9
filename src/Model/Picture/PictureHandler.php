<?php

namespace App\Model\Picture;

use App\Entity\Picture;
use App\Repository\FavoritesRepository;

class PictureHandler
{

    protected $favoritesRepository;

    /**
     * UserHandler constructor.
     * @param FavoritesRepository $favoritesRepository
     */
    public function __construct(
        FavoritesRepository $favoritesRepository
    )
    {
        $this->favoritesRepository = $favoritesRepository;
    }

    public function getOnePicture( array $data)
    {
        $id = $data['name'] ?? "";
        $thumbnail = $data['thumbnail'] ?? "";
        $title = $data['title'] ?? "";
        $author = $data['author'] ?? "";
        $createdUtc = $data['created_utc'] ?? time();

        if (empty($thumbnail)) {
            $thumbnail = $data['url'] ?? "";
        }

        $countFavorites = $this->favoritesRepository->findByFavoritesCountUser($id);

        $picture = new Picture();
        $picture->setId($id);
        $picture->setThumbnail($thumbnail);
        $picture->setTitle($title);
        $picture->setAuthor($author);
        $picture->setCreatedUtc(date('d/m/Y', $createdUtc));
        $picture->setCountFavorites($countFavorites[0][1] ?? 0);

        return $picture;
    }

    public function getListPictures(array $data)
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = $this->getOnePicture($item['data']);
        }

        return $result;
    }
}