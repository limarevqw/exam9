<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_SEARCH_REDDIT = 'https://www.reddit.com/r/picture/search.json';
    const ENDPOINT_INFO_REDDIT = 'https://www.reddit.com/api/info.json';

    /**
     * @param string $search
     * @param string $sort
     * @param int $limit
     * @return mixed
     * @throws ApiException
     */
    public function searchRedditPicture(
        string $search,
        string $sort = 'new',
        int $limit = 5
    )
    {
        return $this->makeQuery(
            self::ENDPOINT_SEARCH_REDDIT,
            self::METHOD_GET,
            [
                'q' => $search,
                'sort' => $sort,
                'limit' => $limit,
                'type' => 'link'
            ]);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws ApiException
     */
    public function oneRedditPicture(
        string $id
    )
    {
        return $this->makeQuery(
            self::ENDPOINT_INFO_REDDIT,
            self::METHOD_GET,
            [
                'id' => $id,
            ]);
    }

    /**
     * @param array $arrayId
     * @return mixed
     * @throws ApiException
     */
    public function listRedditPicture(
        array $arrayId
    )
    {
        return $this->makeQuery(
            self::ENDPOINT_INFO_REDDIT,
            self::METHOD_GET,
            [
                'id' => implode(", ",$arrayId),
            ]);
    }
}
