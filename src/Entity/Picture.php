<?php

namespace App\Entity;

class Picture
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $createdUtc;

    /**
     * @var string
     */
    private $countFavorites;

    /**
     * @param string $id
     * @return Picture
     */
    public function setId(string $id): Picture
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $thumbnail
     * @return Picture
     */
    public function setThumbnail(string $thumbnail): Picture
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @param string $title
     * @return Picture
     */
    public function setTitle(string $title): Picture
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $author
     * @return Picture
     */
    public function setAuthor(string $author): Picture
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $createdUtc
     * @return Picture
     */
    public function setCreatedUtc(string $createdUtc): Picture
    {
        $this->createdUtc = $createdUtc;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedUtc(): string
    {
        return $this->createdUtc;
    }

    /**
     * @param string $countFavorites
     * @return Picture
     */
    public function setCountFavorites(string $countFavorites): Picture
    {
        $this->countFavorites = $countFavorites;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountFavorites(): string
    {
        return $this->countFavorites;
    }
}