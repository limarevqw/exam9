<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Favorites
 * @ORM\Entity(repositoryClass="App\Repository\FavoritesRepository")
 */
class Favorites
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="favoritePictures")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $pictureId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $dataFreddit;

    /**
     * @param int $id
     * @return Favorites
     */
    public function setId(int $id): Favorites
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param \DateTime $createdAt
     * @return Favorites
     */
    public function setCreatedAt(\DateTime $createdAt): Favorites
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param User $user
     * @return Favorites
     */
    public function setUser(User $user): Favorites
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param string $pictureId
     * @return Favorites
     */
    public function setPictureId(string $pictureId): Favorites
    {
        $this->pictureId = $pictureId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPictureId(): string
    {
        return $this->pictureId;
    }

    /**
     * @param mixed $dataFreddit
     * @return Favorites
     */
    public function setDataFreddit($dataFreddit): Favorites
    {
        $this->dataFreddit = $dataFreddit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataFreddit()
    {
        return $this->dataFreddit;
    }


}