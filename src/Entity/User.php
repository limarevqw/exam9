<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Favorites", mappedBy="user")
     */
    private $favoritePictures;

    public function __construct()
    {
        parent::__construct();
        $this->favoritePictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $favoritePictures
     * @return User
     */
    public function setFavoritePictures($favoritePictures)
    {
        $this->favoritePictures = $favoritePictures;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavoritePictures()
    {
        return $this->favoritePictures;
    }
}