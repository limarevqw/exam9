<?php

namespace App\Repository;

use App\Entity\Favorites;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favorites|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorites|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorites[]    findAll()
 * @method Favorites[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoritesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favorites::class);
    }

    public function findByAllUsersFavorites()
    {
        return $this->createQueryBuilder('f')
            ->select('f.pictureId, COUNT(f.pictureId) AS HIDDEN b')
            ->groupBy('f.pictureId')
            ->orderBy('b', "desc")
            ->getQuery()
            ->getResult();
    }

    public function findByUserFavorites(User $user)
    {
        return $this->createQueryBuilder('f')
            ->select('f')
            ->where('f.user = :user')
            ->orderBy('f.createdAt', 'desc')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

    }

    public function findByFavoritesCountUser($pictureId)
    {
        return $this->createQueryBuilder('f')
            ->select('count(f)')
            ->where('f.pictureId = :pictureId')
            ->setParameter('pictureId', $pictureId)
            ->getQuery()
            ->getResult();

    }
}
