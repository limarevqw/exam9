<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractBaseController extends Controller
{
    protected function renderOutput($view, array $parameters = array(), Response $response = null)
    {
        $data = [

        ];

        $parameters = array_merge($parameters,$data);

        return $this->render($view,$parameters,$response);
    }
}