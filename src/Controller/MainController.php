<?php

namespace App\Controller;

use App\Entity\Favorites;
use App\Entity\User;
use App\Form\FilterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Picture\PictureHandler;
use App\Repository\FavoritesRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractBaseController
{
    /**
     * @Route("/", name="homePage")
     *
     * @param ApiContext $apiContext
     * @param PictureHandler $pictureHandler
     * @param Request $request
     * @return Response
     */
    public function indexAction(
        ApiContext $apiContext,
        PictureHandler $pictureHandler,
        Request $request
    )
    {
        $pictures = [];
        $error = null;
        $search = 'cat';
        $sort = 'hot';
        $limit = '8';

        $formFilter = $this->createForm(FilterType::class);
        $formFilter->handleRequest($request);

        if ($formFilter->isSubmitted() && $formFilter->isValid()) {
            $data = $formFilter->getData();
            $search = $data['q'];
            $sort = $data['sort'];
            $limit = $data['limit'];
        }

        try {
            $result = $apiContext->searchRedditPicture($search, $sort, $limit);
            $pictures = $pictureHandler->getListPictures($result['data']['children']);
        } catch (ApiException $e) {
            $error = $e->getMessage();
        }

        return $this->renderOutput("main/index.html.twig", [
            'pictures' => $pictures,
            'error' => $error,
            'formFilter' => $formFilter->createView()
        ]);
    }

    /**
     * @Route("/show/{id}", name="app_main_show")
     *
     * @param ApiContext $apiContext
     * @param PictureHandler $pictureHandler
     * @param string $id
     * @return Response
     */
    public function showAction(
        ApiContext $apiContext,
        PictureHandler $pictureHandler,
        string $id)
    {
        $picture = [];
        $error = null;

        try {
             $result = $apiContext->oneRedditPicture($id);
             $picture = $pictureHandler->getOnePicture($result['data']['children'][0]['data']);

        } catch (ApiException $e) {
            $error = $e->getMessage();
        }
        return $this->renderOutput("main/show.html.twig", [
            'picture' => $picture,
            'error' => $error
        ]);
    }

    /**
     * @Route("/my-favorites", name="app_main_my_favorites")
     *
     * @param PictureHandler $pictureHandler
     * @param Request $request
     * @param ApiContext $apiContext
     * @param FavoritesRepository $favoritesRepository
     * @return Response
     */
    public function myFavoritesAction(
        PictureHandler $pictureHandler,
        Request $request,
        ApiContext $apiContext,
        FavoritesRepository $favoritesRepository
)
    {
        $error = null;

        /** @var User $user */
        $user = $this->getUser();

        /** @var Favorites[] $myFavorites */
        $myFavorites = $favoritesRepository->findByUserFavorites($user);

        try {
            $listMyFavorites = [];

            if ($myFavorites) {
                foreach ($myFavorites as $item) {
                    $result = $apiContext->oneRedditPicture($item->getPictureId());
                    if ($result) {
                        $item->setDataFreddit($pictureHandler->getOnePicture($result['data']['children'][0]['data']));
                    }
                    $listMyFavorites[] = $item;
                }
            }

        } catch (ApiException $e) {
            $error = $e->getMessage();
        }

        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);
        $favoritesResult = $paginator->paginate(
            $listMyFavorites,
            $page,
            $request->query->getInt("limit", 4)
        );

        return $this->renderOutput("main/my_favorites.html.twig", [
            'favoritesResult' => $favoritesResult,
            'error' => $error,

        ]);
    }

    /**
     * @Route("/add-my-favorites/{id}", name="app_main_add_my_favorites")
     *
     * @param ObjectManager $manager
     * @param string $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addMyFavorites(
        ObjectManager $manager,
        string $id)
    {
        $favorites = new Favorites();

        $favorites->setPictureId($id);
        $favorites->setUser($this->getUser());
        $favorites->setCreatedAt(new \DateTime());
        $manager->persist($favorites);
        $manager->flush();

        return $this->redirectToRoute("app_main_my_favorites");
    }

    /**
     * @Route("/selected-communities", name="app_main_selected_communities")
     *
     * @param Request $request
     * @param PictureHandler $pictureHandler
     * @param ApiContext $apiContext
     * @param FavoritesRepository $favoritesRepository
     * @return Response
     */
    public function selectedCommunitiesAction(
        Request $request,
        PictureHandler $pictureHandler,
        ApiContext $apiContext,
        FavoritesRepository $favoritesRepository)
    {
        $favorites = $favoritesRepository->findByAllUsersFavorites();

        $error = null;
        $listMyFavorites = [];

        try {

            if ($favorites) {
                foreach ($favorites as $item) {
                    $result = $apiContext->oneRedditPicture($item['pictureId']);
                    if ($result) {
                        $item['dataFreddit'] = $pictureHandler->getOnePicture($result['data']['children'][0]['data']);
                    }
                    $listMyFavorites[] = $item;
                }
            }

        } catch (ApiException $e) {
            $error = $e->getMessage();
        }


        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);
        $favoritesResult = $paginator->paginate(
            $listMyFavorites,
            $page,
            $request->query->getInt("limit", 8)
        );

        return $this->renderOutput("main/selected_communities.html.twig", [
            'favoritesResult' => $favoritesResult,
            'error' => $error
        ]);
    }

    /**
     * @Route("/test", name="test")
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Response
     */
    public function testAction(Request $request, UserRepository $userRepository)
    {
        $users = $userRepository->findAll();

        $listUsers = [];
        foreach ($users as $item) {
            $item->setUsername([
                'maks' => '34r34r43r'
            ]);
            $listUsers[] = $item;
        }

        $paginator = $this->get('knp_paginator');

        $page = $request->query->getInt("page", 1);
        $usersResult = $paginator->paginate(
            $listUsers,
            $page,
            $request->query->getInt("limit", 1)
        );


        return $this->renderOutput("main/test.html.twig", [
            'usersResult' => $usersResult
        ]);
    }

}